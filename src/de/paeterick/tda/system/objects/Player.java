package de.paeterick.tda.system.objects;

import java.awt.image.BufferedImage;

import de.paeterick.piframework.game.IUpdateable;
import de.paeterick.piframework.math.MathHelper;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.tda.system.content.sprites.ISprite;

public class Player extends GameObject implements IUpdateable {

	private Vector2 velocity = Vector2.emptyVector();
	private Vector2 target = Vector2.emptyVector();
	
	private final ISprite sprite;
	private boolean walking = false;
	
	public Player(final ISprite test) {
		this.sprite = test;
		//halfDimension = new SizeF(((float)img.getWidth() * 0.5f), ((float)img.getHeight() * 0.5f));
	}
	
	public void moveTo(Vector2 pos) {
		walking = true;
		
		velocity = MathHelper.velocityBetween(center, pos);
		velocity.normalize();
		
		if (velocity.y < 0) {
			// UP
			if (velocity.x < 0) {
				// LEFT
				if (velocity.y < velocity.x) {
					// UP
					animate("walk_up");
				} else {
					// LEFT
					animate("walk_left");
				}
			} else if (velocity.x > 0) {
				// RIGHT
				if (Math.abs(velocity.y) > velocity.x) {
					// UP
					animate("walk_up");
				} else {
					// RIGHT
					animate("walk_right");
				}
			}
		} else if (velocity.y > 0) {
			//DOWN
			if (velocity.x < 0) {
				// LEFT
				if (velocity.y < Math.abs(velocity.x)) {
					// LEFT
					animate("walk_left");
				} else {
					// DOWN
					animate("walk_down");
				}
			} else if (velocity.x > 0) {
				// RIGHT
				if (velocity.y > velocity.x) {
					// DOWN
					animate("walk_down");
				} else {
					// RIGHT
					animate("walk_right");
				}
			}
		}
		
		velocity.x *= 2.5f;
		velocity.y *= 2.5f;
		target = pos;
	}
	
	@Override
	public void update(long time) {
		center.x += velocity.x;
		center.y += velocity.y;
		
		boolean stop = false;
		if (velocity.x < 0 && center.x < target.x) {
			center.x = target.x;
			stop = true;
		} else if (velocity.x > 0 && center.x > target.x) {
			center.x = target.x;
			stop = true;
		}
		if (velocity.y < 0 && center.y < target.y) {
			center.y = target.y;
			stop = true;
		} else if (velocity.y > 0 && center.y > target.y) {
			center.y = target.y;
			stop = true;
		}
		
		if (stop && walking) {
			walking = false;
			next("idle");
		}
		
		sprite.update(time);
	}
	
	private void animate(final String name) {
		sprite.animate(name);
		halfDimension = sprite.getHalfDimension();
	}
	
	private void next(final String name) {
		sprite.next(name);
		halfDimension = sprite.getHalfDimension();
	}

	@Override
	public BufferedImage getImage() {
		return sprite.getImage();
	}
	
}