package de.paeterick.tda.system.objects;

import java.awt.image.BufferedImage;

import de.paeterick.piframework.physics.IPhysicsObject;

public interface IGameObject extends IPhysicsObject {
	BufferedImage getImage();
}
