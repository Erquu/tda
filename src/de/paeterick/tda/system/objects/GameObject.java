package de.paeterick.tda.system.objects;

import de.paeterick.piframework.math.SizeF;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.piframework.physics.PhysicsObject;

public abstract class GameObject extends PhysicsObject implements IGameObject {
	
	public GameObject() {
		super();
	}
	public GameObject(final String name) {
		super(name);
	}
	public GameObject(Vector2 center, SizeF halfDimension) {
		super(center, halfDimension);
	}
	public GameObject(final String name, Vector2 center, SizeF halfDimension) {
		super(name, center, halfDimension);
	}
	
	public void setCenter(Vector2 center) {
		this.center = center;
	}
}
