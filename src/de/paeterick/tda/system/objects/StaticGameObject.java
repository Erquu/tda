package de.paeterick.tda.system.objects;

import java.awt.image.BufferedImage;

import de.paeterick.piframework.math.SizeF;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.tda.system.content.store.ImageStore;

public class StaticGameObject extends GameObject {
	
	private final String spritePath;

	public StaticGameObject(final String spritePath) {
		super();
		this.spritePath = spritePath;
	}
	public StaticGameObject(final String spritePath, final String name) {
		super(name);
		this.spritePath = spritePath;
	}
	public StaticGameObject(final String spritePath, Vector2 center, SizeF halfDimension) {
		super(center, halfDimension);
		this.spritePath = spritePath;
	}
	public StaticGameObject(final String spritePath, final String name, Vector2 center, SizeF halfDimension) {
		super(name, center, halfDimension);
		this.spritePath = spritePath;
	}
	
	@Override
	public BufferedImage getImage() {
		return ImageStore.get(spritePath);
	}

}
