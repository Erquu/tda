package de.paeterick.tda.system.content.store;

import java.awt.image.BufferedImage;

import java.util.HashMap;

import de.paeterick.tda.resources.ResourceLoader;

public final class ImageStore {
	private static final HashMap<String, BufferedImage> imageStore = new HashMap<String, BufferedImage>();
	private static ResourceLoader spriteLoader = null;
	
	public static void initialize(final ResourceLoader spriteLoader) {
		ImageStore.spriteLoader = spriteLoader;
	}
	
	public static BufferedImage get(String path) {
		if (spriteLoader != null && !imageStore.containsKey(path)) {
			BufferedImage img = spriteLoader.getImage(path);
			imageStore.put(path, img);
		}
		return imageStore.get(path);
	}
}
