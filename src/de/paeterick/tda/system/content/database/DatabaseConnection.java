package de.paeterick.tda.system.content.database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import de.paeterick.piframework.game.console.GameConsole;
import de.paeterick.tda.resources.ResourceLoader;

public class DatabaseConnection implements Runnable {
	
	private static final boolean useTempDatabase = false;
	private static String DBPATH;
	
	private Connection connection;
	
	public DatabaseConnection(final GameConsole console, final ResourceLoader loader) {
		loader.setFile("instance");
		if (useTempDatabase) {
			try {
				final File tmpPath = File.createTempFile("instance", ".tmp");
				tmpPath.deleteOnExit();
				DatabaseConnection.DBPATH = tmpPath.getAbsolutePath();
				OutputStream out = new FileOutputStream(tmpPath);
				
				final InputStream in = loader.getResourceAsStream();
				int cByte;
				while ((cByte = in.read()) != -1) {
					out.write(cByte);
				}
				in.close();
				out.close();
				
				System.out.println("Created temporary database in " + DatabaseConnection.DBPATH);
				console.addMessage("[^4]Created temporary database: %s", DatabaseConnection.DBPATH);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			String path = loader.getResourceURL().getFile();
			if (path != null) {
				if (path.contains(":") && path.startsWith("/")) {
					path = path.substring(1);
				}
				DatabaseConnection.DBPATH = path;
			}
		}
		
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:" + DatabaseConnection.DBPATH);
			if (!connection.isClosed()) {
				console.addMessage("Database connection established");
			}
		} catch (SQLException e) {
			console.addMessage("[^2]Could not connect to database!");
		}
		
		Runtime.getRuntime().addShutdownHook(new Thread(this));
	}
	
	public Connection getConnection() {
		return connection;
	}
	
	public ResultSet executeQuery(String query) throws SQLException {
		final Statement statement = connection.createStatement();
		return statement.executeQuery(query);
	}
	
	public void executeUpdate(String query) throws SQLException {
		final Statement statement = connection.createStatement();
		statement.executeUpdate(query);
	}
	public void executeUpdate(String format, Object... args) throws SQLException {
		executeUpdate(String.format(format, args));
	}

	@Override
	public void run() {
		try {
			if (!connection.isClosed() && connection != null) {
				connection.close();
				if (connection.isClosed()) {
					System.out.println("DatabaseConnection.java - Database connection closed");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
