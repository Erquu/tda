package de.paeterick.tda.system.content.pipeline;

import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import de.paeterick.piframework.game.graphics.Viewport;
import de.paeterick.piframework.math.MathHelper;
import de.paeterick.piframework.math.SizeF;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.tda.TDA;
import de.paeterick.tda.system.Map;
import de.paeterick.tda.system.objects.IGameObject;
import de.paeterick.tda.system.objects.StaticGameObject;

public class GameObjectPipeline implements IObjectPipeline {
	
	private final HashMap<Integer, HashMap<Integer, Vector<IGameObject>>> test = new HashMap<Integer, HashMap<Integer, Vector<IGameObject>>>();
	private final List<IGameObject> objectsInRange = new Vector<IGameObject>();
	
	private int lastCoordX = 0;
	private int lastCoordY = 0;
	private int lastTargetX = 0;
	private int lastTargetY = 0;
	
	// STATS
	long lastLoadingTimeNeeded = 0;
	
	public GameObjectPipeline() {
		
		final String[] plants = new String[] {
			"plants/tree_01.png",
			"plants/tree_02.png",
			"plants/tree_03.png",
			"plants/tree_04.png",
			"plants/tree_05.png",
			"plants/tree_06.png",
		};
		
		Vector2 pos;
		float size;
		for (int i = 0; i < 30000; i++) {
			pos = MathHelper.randomVector2(0, 25600, 0, 25600);
			size = 64f;
			
			add(new StaticGameObject(plants[MathHelper.randomInteger(0, plants.length)], "Tree", pos, new SizeF(size, size)));
		}
	}
	
	public void add(IGameObject obj) {
		Vector2 pos = obj.getCenter();
		/*SizeF halfSize = obj.getHalfDimension();
		
		float n = pos.y - halfSize.height;
		float e = pos.x + halfSize.width;
		float s = pos.y + halfSize.height;
		float w = pos.x - halfSize.width;
		 
		int coordN = (int) Math.floor((double)n / Map.TILESIZE_Y);
		int coordE = (int) Math.floor((double)e / Map.TILESIZE_X); 
		int coordS = (int) Math.floor((double)s / Map.TILESIZE_Y);
		int coordW = (int) Math.floor((double)w / Map.TILESIZE_X);*/

		int coordX = (int) Math.floor((double)pos.x / Map.TILESIZE_X); 
		int coordY = (int) Math.floor((double)pos.y / Map.TILESIZE_Y);
		
		//System.out.println(String.format("Pipelne: Added Object to X:%s Y:%s", coordX, coordY));
		
		if (!test.containsKey(coordX)) {
			test.put(coordX, new HashMap<Integer, Vector<IGameObject>>());
		}
		if (!test.get(coordX).containsKey(coordY)) {
			test.get(coordX).put(coordY, new Vector<IGameObject>());
		}
		test.get(coordX).get(coordY).add(obj);
	}

	@Override
	public List<IGameObject> getObjectsInView(Viewport viewport) {
		
		boolean loading = false;
		
		long start = System.nanoTime();
		
		// Round to tilesize
		int coordX = (int) Math.floor((double)viewport.x / Map.TILESIZE_X) - 1; 
		int coordY = (int) Math.floor((double)viewport.y / Map.TILESIZE_Y) - 1;
		
		int targetX = coordX + (int) Math.ceil((double)viewport.width / Map.TILESIZE_X) + 1;
		int targetY = coordY + (int) Math.ceil((double)viewport.height / Map.TILESIZE_Y) + 1;
		
		if (coordX != lastCoordX || coordY != lastCoordY || targetX != lastTargetX || targetY != lastTargetY) {
			objectsInRange.clear();
			
			for (int x = coordX; x <= targetX; x++) {
				for (int y = coordY; y <= targetY; y++) {
					if (test.containsKey(x) && test.get(x).containsKey(y)) {
						List<IGameObject> objects = test.get(x).get(y);
						for (IGameObject o : objects) {
							objectsInRange.add(o);
						}
					}
				}
			}
			
			lastCoordX = coordX;
			lastCoordY = coordY;
			lastTargetX = targetX;
			lastTargetY = targetY;
			
			loading = true;
			
			lastLoadingTimeNeeded = (System.nanoTime() - start); 
		}
		
		
		if (TDA.ENGINE != null) {
			TDA.ENGINE.addDebugString("Pipeline (%s):", (loading?"loading":"not loading"));
			TDA.ENGINE.addDebugString(String.format("   Time: %sms (%sns)", lastLoadingTimeNeeded / 1000000, lastLoadingTimeNeeded));
			TDA.ENGINE.addDebugString(String.format("   Loaded %s Objects", objectsInRange.size()));
			TDA.ENGINE.addDebugString(String.format("   Range from x:%s y:%s to x:%s y:%s %s", coordX, coordY, targetX, targetY, viewport));
		}
		
		return objectsInRange;
	}

	@Override
	public void forceReload(int preloadTiles) {
		// TODO Auto-generated method stub
		
	}
}