package de.paeterick.tda.system.content.pipeline;

import java.awt.Point;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import de.paeterick.piframework.game.graphics.Viewport;
import de.paeterick.piframework.math.SizeF;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.tda.TDA;
import de.paeterick.tda.system.Map;
import de.paeterick.tda.system.content.database.DatabaseConnection;
import de.paeterick.tda.system.objects.IGameObject;
import de.paeterick.tda.system.objects.StaticGameObject;

public class CachingDBObjectPipeline implements IObjectPipeline, Runnable {
	
	private static final int preloadedTiles = 1;
	
	private final List<Point> toLoad = new Vector<Point>();
	private final List<IGameObject> objectsInRange = new Vector<IGameObject>();
	private final java.util.Map<Integer, java.util.Map<Integer, List<IGameObject>>> tiles = new HashMap<Integer, java.util.Map<Integer, List<IGameObject>>>();

	private final DatabaseConnection connection;
	
	// Prevent loading when no new tiles are displayed
	private int lastCoordX = 0;
	private int lastCoordY = 0;
	private int lastTargetX = 0;
	private int lastTargetY = 0;
	
	private boolean forceReload = false;
	private boolean forceReloaded = false;
	
	/*private int preloadFromX = 0;
	private int preloadFromY = 0;
	private int preloadToX = 0;
	private int preloadToY = 0;*/
	
	private boolean preloading = false;
	
	// STATS
	private long lastLoadingTimeNeeded = 0;
	private long lastPreLoadingTimeNeeded = 0;
	private int preloadedObjects = 0;
	
	public CachingDBObjectPipeline(final DatabaseConnection connection) {
		this.connection = connection;
	}

	@Override
	public List<IGameObject> getObjectsInView(Viewport viewport) {
		
		boolean loading = false;
		
		// Round to tilesize
		int coordX = (int) Math.floor((double)viewport.x / Map.TILESIZE_X); 
		int coordY = (int) Math.floor((double)viewport.y / Map.TILESIZE_Y);
		int targetX = coordX + (int) Math.ceil((double)viewport.width / Map.TILESIZE_X);
		int targetY = coordY + (int) Math.ceil((double)viewport.height / Map.TILESIZE_Y);
		
		if ((forceReload && forceReloaded) || coordX != lastCoordX || coordY != lastCoordY || targetX != lastTargetX || targetY != lastTargetY) {
			
			if (forceReload && forceReloaded) {
				forceReload = forceReloaded = false;
			}
			
			long start = System.nanoTime();
			
			/* UNLOADING */
			
			int movementX = coordX - lastCoordX;
			int movementY = coordY - lastCoordY;
			int movAbsX = Math.abs(movementX);
			int movAbsY = Math.abs(movementY);
			
			/*preloadFromX = coordX - preloadedTiles;
			preloadFromY = coordY - preloadedTiles;
			preloadToX = targetX + preloadedTiles;
			preloadToY = targetY + preloadedTiles;*/
			
			int unloadX = -10;
			int unloadY = -10;
			
			int loadX = coordX;
			int loadY = coordY;
			
			if (movementX < 0) {
//				System.out.println("Moved left for " + movAbsX + " tile");
				unloadX = lastTargetX + movAbsX;
				loadX = coordX - movAbsX;
			} else if (movementX > 0) {
//				System.out.println("Moved right for " + movAbsX + " tile");
				unloadX = lastCoordX - movAbsX;
				loadX = targetX + movAbsX;
			}
			if (movementY < 0) {
//				System.out.println("Moved up for " + movAbsY + " tile");
				unloadY = lastTargetY + movAbsY;
				loadY = coordY - movAbsY;
			} else if (movementY > 0) {
//				System.out.println("Moved down for " + movAbsY + " tile");
				unloadY = lastCoordY - movAbsY;
				loadY = targetY + movAbsY;
			}
			
			for (int x = unloadX; x <= unloadX + (lastTargetX - lastCoordX); x++) {
				if (tiles.containsKey(x) && tiles.get(x).containsKey(unloadY)) {
					tiles.get(x).put(unloadY, null);
				}
			}
			
			for (int y = unloadY + 1; y <= unloadY + (lastTargetY - lastCoordY); y++) { // TODO: Check if +1 is correct
				if (tiles.containsKey(unloadX) && tiles.get(unloadX).containsKey(y)) {
					tiles.get(unloadX).put(y, null);
				}
			}
			
			/* END OF UNLOADING */
			
			if (!preloading) {
//				System.out.println("___________________________________");
				for (int x = loadX; x <= loadX + (targetX - coordX); x++) {
//					System.out.println(String.format("Loading x:%s y:%s", x, loadY));
					toLoad.add(new Point(x, loadY));
				}
				for (int y = loadY + 1; y <= loadY + (targetY - coordY); y++) {
//					System.out.println(String.format("Loading x:%s y:%s", loadX, y));
					toLoad.add(new Point(loadX, y));
				}
				
				(new Thread(this)).start();
			}
			
			objectsInRange.clear();
			for (int x = coordX; x <= targetX; x++) {
				for (int y = coordY; y <= targetY; y++) {
					if (tiles.containsKey(x) && tiles.get(x).containsKey(y) && tiles.get(x).get(y) != null) {
						for (IGameObject obj : tiles.get(x).get(y)) {
							objectsInRange.add(obj);
						}
					}
				}
			}
			
			loading = true;
			
			lastLoadingTimeNeeded = (System.nanoTime() - start); 
		}
		
		lastCoordX = coordX;
		lastCoordY = coordY;
		lastTargetX = targetX;
		lastTargetY = targetY;
		
		if (TDA.ENGINE != null) {
			TDA.ENGINE.addDebugString("Pipeline (%s):", (loading?"loading":"not loading"));
			TDA.ENGINE.addDebugString(String.format("   Preloading: %sms (%sns)", lastPreLoadingTimeNeeded / 1000000, lastPreLoadingTimeNeeded));
			TDA.ENGINE.addDebugString(String.format("   Preloaded %s Objects", preloadedObjects));
			TDA.ENGINE.addDebugString(String.format("   Loading: %sms (%sns)", lastLoadingTimeNeeded / 1000000, lastLoadingTimeNeeded));
			TDA.ENGINE.addDebugString(String.format("   Loaded %s Objects", objectsInRange.size()));
			TDA.ENGINE.addDebugString(String.format("   Range from x:%s y:%s to x:%s y:%s %s", coordX, coordY, targetX, targetY, viewport));
		}
		
		return objectsInRange;
	}

	@Override
	public void run() {
		try {
			long start = System.nanoTime();
			
			preloadedObjects = 0;
			preloading = true;
			synchronized (toLoad) {
				for (Point p : toLoad) {
					ResultSet resultSet = connection.executeQuery(String.format("SELECT * FROM OBJECTS WHERE POSX >= %s AND POSY >= %s AND POSX <= %s AND POSY <= %s ORDER BY POSY",
						p.x * Map.TILESIZE_X,
						p.y * Map.TILESIZE_Y,
						(p.x + 1) * Map.TILESIZE_X,
						(p.y + 1) * Map.TILESIZE_Y));
					
					while (resultSet.next()) {
						int posX = resultSet.getInt(2);
						int posY = resultSet.getInt(3);
						String spritePath = resultSet.getString(4);
						
						StaticGameObject obj = new StaticGameObject(spritePath, "Tree", new Vector2(posX, posY), new SizeF(32, 64));
						if (!tiles.containsKey(p.x))
							tiles.put(p.x, new HashMap<Integer, List<IGameObject>>());
						if (!tiles.get(p.x).containsKey(p.y) || tiles.get(p.x).get(p.y) == null)
							tiles.get(p.x).put(p.y, new Vector<IGameObject>());
						tiles.get(p.x).get(p.y).add(obj);
						preloadedObjects++;
					}
				}
				toLoad.clear();
			}
			preloading = false;
			
			lastPreLoadingTimeNeeded = (System.nanoTime() - start);
			
		} catch (SQLException ex) {
			
		}
		
		forceReloaded = true;
	}

	@Override
	public void forceReload(int preloadTiles) {
		preloadTiles = Math.max(preloadTiles, preloadedTiles);
		synchronized (toLoad) {
			for (int x = lastCoordX - preloadTiles; x <= lastTargetX + preloadTiles; x++) {
				for (int y = lastCoordY - preloadTiles; y <= lastTargetY + preloadTiles; y++) {
					toLoad.add(new Point(x, y));
				}
			}
		}
		
		forceReloaded = false;
		forceReload = true;
	}
}
