package de.paeterick.tda.system.content.pipeline;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

import de.paeterick.piframework.game.graphics.Viewport;
import de.paeterick.piframework.math.SizeF;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.tda.TDA;
import de.paeterick.tda.system.Map;
import de.paeterick.tda.system.content.database.DatabaseConnection;
import de.paeterick.tda.system.objects.IGameObject;
import de.paeterick.tda.system.objects.StaticGameObject;

public class DBObjectPipeline implements IObjectPipeline {

	private final List<IGameObject> objectsInRange = new Vector<IGameObject>();
	
	private final DatabaseConnection connection;
	
	private int lastCoordX = 0;
	private int lastCoordY = 0;
	private int lastTargetX = 0;
	private int lastTargetY = 0;
	
	// STATS
	long lastLoadingTimeNeeded = 0;
	
	public DBObjectPipeline(final DatabaseConnection connection) {
		this.connection = connection;
	}

	@Override
	public List<IGameObject> getObjectsInView(Viewport viewport) {
		try {
			boolean loading = false;
			
			long start = System.nanoTime();
			
			// Round to tilesize
			int coordX = (int) Math.floor((double)viewport.x / Map.TILESIZE_X) - 1; 
			int coordY = (int) Math.floor((double)viewport.y / Map.TILESIZE_Y) - 1;
			
			int targetX = coordX + (int) Math.ceil((double)viewport.width / Map.TILESIZE_X) + 1;
			int targetY = coordY + (int) Math.ceil((double)viewport.height / Map.TILESIZE_Y) + 1;
			
			coordX--;
			coordY--;
			targetX += 2;
			targetY += 2;
			
			if (coordX != lastCoordX || coordY != lastCoordY || targetX != lastTargetX || targetY != lastTargetY) {
				objectsInRange.clear();
				
				/*ResultSet resultSet = connection.executeQuery(
						String.format(
								"SELECT * FROM OBJECTS WHERE POSX >= %s AND POSY >= %s AND POSX <= %s AND POSY <= %s ORDER BY POSY",
								viewport.x,
								viewport.y,
								viewport.x + viewport.width,
								viewport.y + viewport.height));*/
				
				ResultSet resultSet = connection.executeQuery(
						String.format(
								"SELECT * FROM OBJECTS WHERE POSX >= %s AND POSY >= %s AND POSX <= %s AND POSY <= %s ORDER BY POSY",
								coordX * Map.TILESIZE_X,
								coordY * Map.TILESIZE_Y,
								targetX * Map.TILESIZE_X,
								targetY * Map.TILESIZE_Y));
				
				while (resultSet.next()) {
					int posX = resultSet.getInt(2);
					int posY = resultSet.getInt(3);
					String spritePath = resultSet.getString(4);
					
					StaticGameObject obj = new StaticGameObject(spritePath, "Tree", new Vector2(posX, posY), new SizeF(32, 64));
					objectsInRange.add(obj);
				}
				
				lastCoordX = coordX;
				lastCoordY = coordY;
				lastTargetX = targetX;
				lastTargetY = targetY;
				
				loading = true;
				
				lastLoadingTimeNeeded = (System.nanoTime() - start); 
			}
			
			
			if (TDA.ENGINE != null) {
				TDA.ENGINE.addDebugString("Pipeline (%s):", (loading?"loading":"not loading"));
				TDA.ENGINE.addDebugString(String.format("   Time: %sms (%sns)", lastLoadingTimeNeeded / 1000000, lastLoadingTimeNeeded));
				TDA.ENGINE.addDebugString(String.format("   Loaded %s Objects", objectsInRange.size()));
				TDA.ENGINE.addDebugString(String.format("   Range from x:%s y:%s to x:%s y:%s %s", coordX, coordY, targetX, targetY, viewport));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return objectsInRange;
	}

	@Override
	public void forceReload(int preloadTiles) {
		// TODO Auto-generated method stub
		
	}

}
