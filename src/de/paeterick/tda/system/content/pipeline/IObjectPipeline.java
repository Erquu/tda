package de.paeterick.tda.system.content.pipeline;

import java.util.List;

import de.paeterick.piframework.game.graphics.Viewport;
import de.paeterick.tda.system.objects.IGameObject;

public interface IObjectPipeline {
	void forceReload(int preloadTiles);
	List<IGameObject> getObjectsInView(Viewport viewport);
}
