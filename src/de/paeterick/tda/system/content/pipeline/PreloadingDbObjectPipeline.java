package de.paeterick.tda.system.content.pipeline;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import de.paeterick.piframework.game.graphics.Viewport;
import de.paeterick.piframework.math.SizeF;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.tda.TDA;
import de.paeterick.tda.system.Map;
import de.paeterick.tda.system.content.database.DatabaseConnection;
import de.paeterick.tda.system.objects.IGameObject;
import de.paeterick.tda.system.objects.StaticGameObject;

public class PreloadingDbObjectPipeline implements IObjectPipeline, Runnable {
	
	class Point {
		public int x;
		public int y;
		
		public Point() {
			x = y = 0;
		}
		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}
		
		@Override
		public boolean equals(Object obj) {
			return (obj instanceof Point && ((Point)obj).x == x && ((Point)obj).y == y);
		}
		
		@Override
		public String toString() {
			return String.format("Point[x=%s, y=%s]", x, y);
		}
	}
	
	class TileLoadingInformation {
		public final List<Point> toLoad;
		public final List<Point> toUnload;
		
		public TileLoadingInformation(final List<Point> toLoad, final List<Point> toUnload) {
			this.toLoad = toLoad;
			this.toUnload = toUnload;
		}
	}
	


	private final DatabaseConnection connection;
	private final List<IGameObject> objectsInRange = new Vector<IGameObject>();
	private final java.util.Map<Integer, java.util.Map<Integer, List<IGameObject>>> tiles = new HashMap<Integer, java.util.Map<Integer, List<IGameObject>>>();

	// Prevent loading when no new tiles are displayed
	private int lastCoordX = 0;
	private int lastCoordY = 0;
	private int lastTargetX = 0;
	private int lastTargetY = 0;
	
	private boolean forceReload = false;
	private boolean forceReloaded = false;
	
	private TileLoadingInformation tileLoadingInformation;
	
	// STATS
	private long lastLoadingTimeNeeded = 0;
	private long lastPreLoadingTimeNeeded = 0;
	private int preloadedObjects = 0;
	private int unloadedObjects = 0;
	
	public PreloadingDbObjectPipeline(final DatabaseConnection connection) {
		this.connection = connection;
	}

	@Override
	public void forceReload(int preloadTiles) {
		tileLoadingInformation = getTileInformation(lastCoordX, lastCoordY, lastTargetX, lastTargetY, preloadTiles);
		tiles.clear();
		
		forceReloaded = false;
		forceReload = true;
		
		(new Thread(this)).start();
	}

	@Override
	public List<IGameObject> getObjectsInView(Viewport viewport) {
		
		// Round to tilesize
		int coordX = (int) Math.floor((double)viewport.x / Map.TILESIZE_X); 
		int coordY = (int) Math.floor((double)viewport.y / Map.TILESIZE_Y);
		int targetX = coordX + (int) Math.ceil((double)viewport.width / Map.TILESIZE_X);
		int targetY = coordY + (int) Math.ceil((double)viewport.height / Map.TILESIZE_Y);
		
		if ((forceReload && forceReloaded) || coordX != lastCoordX || coordY != lastCoordY || targetX != lastTargetX || targetY != lastTargetY) {
			
			if (forceReloaded) {
				forceReload = false;
				forceReloaded = false;
			}
			
			long start = System.nanoTime();
			
			tileLoadingInformation = getTileInformation(lastCoordX, lastCoordY, lastTargetX, lastTargetY, coordX, coordY, targetX, targetY, 1);
			
			(new Thread(this)).start();
			
			objectsInRange.clear();
			for (int y = coordY; y <= targetY; y++) {
				for (int x = coordX; x <= targetX; x++) {
					if (tiles.containsKey(x) && tiles.get(x).containsKey(y) && tiles.get(x).get(y) != null) {
						for (IGameObject obj : tiles.get(x).get(y)) {
							objectsInRange.add(obj);
						}
					}
				}
			}

			lastLoadingTimeNeeded = (System.nanoTime() - start); 
		}
		
		lastCoordX = coordX;
		lastCoordY = coordY;
		lastTargetX = targetX;
		lastTargetY = targetY;
		
		if (TDA.ENGINE != null) {
			//TDA.ENGINE.addDebugString("Pipeline (%s):", (loading?"loading":"not loading"));
			TDA.ENGINE.addDebugString(String.format("   Preloading: %sms (%sns)", lastPreLoadingTimeNeeded / 1000000, lastPreLoadingTimeNeeded));
			TDA.ENGINE.addDebugString(String.format("     Preloaded %s Objects", preloadedObjects));
			TDA.ENGINE.addDebugString(String.format("     Unloaded %s Objects", unloadedObjects));
			TDA.ENGINE.addDebugString(String.format("   Loading: %sms (%sns)", lastLoadingTimeNeeded / 1000000, lastLoadingTimeNeeded));
			TDA.ENGINE.addDebugString(String.format("     Loaded %s Objects", objectsInRange.size()));
			TDA.ENGINE.addDebugString(String.format("   Range from x:%s y:%s to x:%s y:%s %s", coordX, coordY, targetX, targetY, viewport));
		}
		
		return objectsInRange;
	}
	
	
	private TileLoadingInformation getTileInformation(int fromX, int fromY, int toX, int toY, int preloadedTiles) {
		final List<Point> toLoad = new Vector<Point>();
		
		for (int x = fromX; x <= toX; x++) {
			for (int y = fromY; y <= toY; y++) {
				toLoad.add(new Point(x, y));
			}
		}
		
		return new  TileLoadingInformation(toLoad, new Vector<Point>());
	}
	/**
	 * Returns the tile positions for the tiles which should be loaded and unloaded
	 * @param oldFromX
	 * @param oldFromY
	 * @param oldToX
	 * @param oldToY
	 * @param fromX
	 * @param fromY
	 * @param toX
	 * @param toY
	 * @param preloadedTiles
	 * @return
	 */
	private TileLoadingInformation getTileInformation(int oldFromX, int oldFromY, int oldToX, int oldToY, int fromX, int fromY, int toX, int toY, int preloadedTiles) {
		final int diffFromX = fromX - oldFromX;
		final int diffFromY = fromY - oldFromY;
		
		final List<Point> toLoad = new Vector<Point>();
		final List<Point> toUnload = new Vector<Point>();
		
		Point current;
		
		if (diffFromX < 0) { // MOVE LEFT
			// UNLOAD
			int x = oldToX + preloadedTiles;
			for (int y = (oldFromY - preloadedTiles); y <= (oldToY + preloadedTiles); y++) {
				current = new Point(x, y);
				if (!toUnload.contains(current))
					toUnload.add(current);
			}
			
			// LOAD
			x = fromX - preloadedTiles;
			for (int y = (fromY - preloadedTiles); y <= toY + preloadedTiles; y++) {
				current = new Point(x, y);
				if (!toLoad.contains(current))
					toLoad.add(current);
			}
		} else if (diffFromX > 0) { // MOVE RIGHT
			// UNLOAD
			int x = oldFromX - preloadedTiles;
			for (int y = (oldFromY - preloadedTiles); y <= (oldToY + preloadedTiles); y++) {
				current = new Point(x, y);
				if (!toUnload.contains(current))
					toUnload.add(current);
			}
			
			// LOAD
			x = toX + preloadedTiles;
			for (int y = (fromY - preloadedTiles); y <= toY + preloadedTiles; y++) {
				current = new Point(x, y);
				if (!toLoad.contains(current))
					toLoad.add(current);
			}
		}
		
		if (diffFromY < 0) { // MOVE UP
			// UNLOAD
			int y = oldToY + preloadedTiles;
			for (int x = (oldFromX - preloadedTiles); x <= (oldToX + preloadedTiles); x++) {
				current = new Point(x, y);
				if (!toUnload.contains(current))
					toUnload.add(current);
			}
			
			// LOAD
			y = fromY - preloadedTiles;
			for (int x = (fromX - preloadedTiles); x <= (toX + preloadedTiles); x++) {
				current = new Point(x, y);
				if (!toLoad.contains(current))
					toLoad.add(current);
			}
		} else if (diffFromY > 0) { // MOVE DOWN
			// UNLOAD
			int y = oldFromY - preloadedTiles;
			for (int x = (oldFromX - preloadedTiles); x <= (oldToX + preloadedTiles); x++) {
				current = new Point(x, y);
				if (!toUnload.contains(current))
					toUnload.add(current);
			}

			// LOAD
			y = toY + preloadedTiles;
			for (int x = (fromX - preloadedTiles); x <= (toX + preloadedTiles); x++) {
				current = new Point(x, y);
				if (!toLoad.contains(current))
					toLoad.add(current);
			}
		}
		
		return new TileLoadingInformation(toLoad, toUnload);
	}

	@Override
	public void run() {
		
		long start = System.nanoTime();
		preloadedObjects = 0;
		unloadedObjects = 0;
		
		TileLoadingInformation info = tileLoadingInformation; // Queue?
		
		if (info != null) {
			// UNLOAD
			for (Point p : info.toUnload) {
				if (tiles.containsKey(p.x) && tiles.get(p.x).containsKey(p.y)) {
					unloadedObjects += tiles.get(p.x).get(p.y).size();
					tiles.get(p.x).remove(p.y);
					if (tiles.get(p.x).size() == 0) {
						tiles.remove(p.x);
					}
				}
			}
			
			// LOAD
			int fromX, fromY;
			StaticGameObject obj;
			for (Point p : info.toLoad) {
				try {
					fromX = (int) (p.x * Map.TILESIZE_X);
					fromY = (int) (p.y * Map.TILESIZE_Y);
					ResultSet resultSet = connection.executeQuery(String.format("SELECT * FROM OBJECTS WHERE POSX >= %s AND POSY >= %s AND POSX <= %s AND POSY <= %s ORDER BY POSY",
							fromX,
							fromY,
							fromX + Map.TILESIZE_X,
							fromY + Map.TILESIZE_Y));
					
					while (resultSet.next()) {
						int posX = resultSet.getInt(2);
						int posY = resultSet.getInt(3);
						String spritePath = resultSet.getString(4);
						
						obj = new StaticGameObject(spritePath, "Tree", new Vector2(posX, posY), new SizeF(32, 64));
						
						if (!tiles.containsKey(p.x))
							tiles.put(p.x, new HashMap<Integer, List<IGameObject>>());
						if (!tiles.get(p.x).containsKey(p.y) || tiles.get(p.x).get(p.y) == null)
							tiles.get(p.x).put(p.y, new Vector<IGameObject>());
						
						tiles.get(p.x).get(p.y).add(obj);
						preloadedObjects++;
					}
				} catch (SQLException e) {
					TDA.ENGINE.getConsole().addMessage("[^2]Could not load tile x:%s y:%s", p.x, p.y);
				}
			}
		}
		tileLoadingInformation = null;
		
		lastPreLoadingTimeNeeded = (System.nanoTime() - start);
		
		if (forceReload)
			forceReloaded = true;
	}
}
