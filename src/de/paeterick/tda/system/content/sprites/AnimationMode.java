package de.paeterick.tda.system.content.sprites;

public enum AnimationMode {
	SINGLE,
	REPEAT,
	BACK_AND_FORTH
}
