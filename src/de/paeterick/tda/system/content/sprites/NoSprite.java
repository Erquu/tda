package de.paeterick.tda.system.content.sprites;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import de.paeterick.piframework.math.SizeF;

public class NoSprite implements ISprite {
	
	private static final BufferedImage EMPTYIMAGE = createEmptyImage();
	private static final SizeF NOSPRITE_SIZE = new SizeF(16, 16);
	
	private static BufferedImage createEmptyImage() {
		final BufferedImage img = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
		final Graphics2D context = (Graphics2D)img.getGraphics();
		context.setColor(Color.WHITE);
		context.fillRect(0, 0, 32, 32);
		return img;
	}

	@Override
	public void update(final long time) { }

	@Override
	public void next() { }

	@Override
	public void next(final String name) { }

	@Override
	public void animate(final String name) { }

	@Override
	public String getName() {
		return "EMPTYIMAGE";
	}

	@Override
	public String getDescription() {
		return "EMPTYIMAGE";
	}

	@Override
	public SizeF getHalfDimension() {
		return NOSPRITE_SIZE;
	}

	@Override
	public BufferedImage getImage() {
		return EMPTYIMAGE;
	}
}
