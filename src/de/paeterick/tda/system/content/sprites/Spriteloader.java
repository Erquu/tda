package de.paeterick.tda.system.content.sprites;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import de.paeterick.tda.resources.ResourceLoader;

public class Spriteloader {
	
	private enum Mode {
		NONE,
		ROOT,
		DEFAULTS,
		STATES,
		STATE,
		NEXT
	}
	private enum Var {
		NONE,
		ROOT_NAME,
		ROOT_DESCRIPTION,
		ROOT_PATH
	}
	
	private static class SpriteTemplate {
		public String name = "";
		public String description = "";
		public String path = "";
		public int defaultState = -1;
		
		public final List<State> states = new LinkedList<State>();
		
		public Sprite toSprite() {
			return new Sprite(name, description, path, states, defaultState);
		}
	}
	private static class StateTemplate {
		public String name = "";
		public int length = 1;
		public int startX = 0;
		public int startY = 0;
		public int sizeX = 0;
		public int sizeY = 0;
		public int pause = 60;
		public boolean defaultState = false;
		public AnimationMode animationMode = null;
		public final Map<String, String> next = new HashMap<String, String>();
		
		
		public StateTemplate(final Map<String, String> defaults) {
			final Iterator<String> keys = defaults.keySet().iterator();
			String key;
			while (keys.hasNext()) {
				key = keys.next();
				add(key, defaults.get(key));
			}
		}
		
		public State toState() {
			final State state;
			if (animationMode == null)
				state = new State(name, length, startX, startY, sizeX, sizeY, pause, next);
			else
				state = new State(name, length, startX, startY, sizeX, sizeY, pause, next, animationMode);
			return state;
		}
		
		private void add(final String key, final String value) {
			if ("name".equals(key)) {
				name = value;
			} else if ("length".equals(key)) {
				length = getInt(value, length);
			} else if ("startx".equals(key)) {
				startX = getInt(value, startX);
			} else if ("starty".equals(key)) {
				startY = getInt(value, startY);
			} else if ("sizex".equals(key)) {
				sizeX = getInt(value, sizeX);
			} else if ("sizey".equals(key)) {
				sizeY = getInt(value, sizeY);
			} else if ("pause".equals(key)) {
				pause = getInt(value, pause);
			} else if ("mode".equals(key)) {
				try {
					animationMode = Enum.valueOf(AnimationMode.class, value.toUpperCase());
				} catch (IllegalArgumentException ex) {
				} catch (NullPointerException ex1) {
				}
			}
		}
		
		private int getInt(final String s, final int defaultValue) {
			try {
				return Integer.parseInt(s);
			} catch (NumberFormatException ex) {}
			return defaultValue;
		}
	}
	
	//////////////////////////////////////////////////////////////////	
	
	private static ResourceLoader loader;
	
	public static void initialize(final ResourceLoader loader) {
		Spriteloader.loader = loader;
	}
	
	@SuppressWarnings("incomplete-switch")
	public static ISprite load(final String path) throws XMLStreamException {
		
		boolean validated = false;
		
		final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = null;
		try {
			loader.setFile("sprite.xsd");
			schema = schemaFactory.newSchema(new StreamSource(loader.getResourceAsStream()));
		} catch (SAXException e1) {
			e1.printStackTrace();
		}
		loader.setFile(path);
		if (schema != null) {
			final Validator validator = schema.newValidator();
			
			try {
				validator.validate(new StreamSource(loader.getResourceAsStream()));
				validated = true;
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (validated) {
			final XMLInputFactory factory = XMLInputFactory.newInstance();
			final XMLStreamReader parser = factory.createXMLStreamReader(loader.getResourceAsStream());
			
			final SpriteTemplate template = new SpriteTemplate();
			StateTemplate stateTemplate = null;
			
			Mode mode = Mode.NONE;
			Var var = Var.NONE;
			
			final Map<String, String> defaults = new HashMap<String, String>();
			
			String currentKey = null;
			String name;
			
			while (parser.hasNext()) {
				switch (parser.getEventType()) {
					case XMLStreamConstants.START_DOCUMENT:
						mode = Mode.ROOT;
						break;
					case XMLStreamConstants.END_DOCUMENT:
						parser.close();
						break;
					case XMLStreamConstants.START_ELEMENT:
						name = parser.getLocalName().toLowerCase();
						
						switch (mode) {
							case ROOT:
								if ("name".equals(name)) {
									var = Var.ROOT_NAME;
								} else if ("description".equals(name)) {
									var = Var.ROOT_DESCRIPTION;
								} else if ("path".equals(name)) {
									var = Var.ROOT_PATH;
								} else if ("default".equals(name)) {
									mode = Mode.DEFAULTS;
								} else if ("states".equals(name)) {
									mode = Mode.STATES;
								}
								break;
							case STATES:
								if ("state".equals(name)) {
									mode = Mode.STATE;
									stateTemplate = new StateTemplate(defaults);
									
									for (int i = 0; i < parser.getAttributeCount(); i++) {
										if ("default".equals(parser.getAttributeLocalName(i))) {
											stateTemplate.defaultState = Boolean.parseBoolean(parser.getAttributeValue(i));
										}
									}
								}
							case STATE:
								if ("next".equals(name)) {
									mode = Mode.NEXT;
									break;
								}
								// FALL THROUGH, CONDITION BREAKS IN CASE
							case NEXT:
							case DEFAULTS:
								if ("value".equals(name)) {
									for (int i = 0; i < parser.getAttributeCount(); i++) {
										if ("name".equals(parser.getAttributeLocalName(i).toLowerCase())) {
											currentKey = parser.getAttributeValue(i).toLowerCase();
										}
									}
								}
								break;
						}
						break;
					case XMLStreamConstants.END_ELEMENT:
						name = parser.getLocalName().toLowerCase();
						switch (mode) {
							case STATE:
								if ("state".equals(name)) {
									if (stateTemplate != null) {
										final int index = template.states.size();
										final State tempState = stateTemplate.toState();
										template.states.add(tempState);
										if (stateTemplate.defaultState && tempState.equals(template.states.get(index))) {
											template.defaultState = index;
											System.out.println("Default State is " + tempState.getName());
										}
										stateTemplate = null;
									}
									mode = Mode.STATES;
								}	
								break;
							case NEXT:
								if ("next".equals(name)) {
									mode = Mode.STATE;
								}
								break;
							case STATES:
								if ("states".equals(name)) {
									mode = Mode.ROOT;
								}	
								break;
							case DEFAULTS:
								if ("default".equals(name)) {
									mode = Mode.ROOT;
								}	
								break;
							case ROOT:
								if ("name".equals(name) ||
									"description".equals(name) ||
									"path".equals(name)) {
									var = Var.NONE;
								}
								break;
						}
						break;
					case XMLStreamConstants.CHARACTERS:
						String text = parser.getText();
						if (!parser.isWhiteSpace()) {
							switch (mode) {
								case ROOT:
									switch (var) {
										case ROOT_NAME:
											template.name = text.toLowerCase();
											break;
										case ROOT_DESCRIPTION:
											template.description = text;
											break;
										case ROOT_PATH:
											template.path = text;
											break;
									}
									break;
								case DEFAULTS:
									if (currentKey != null) {
										defaults.put(currentKey, text.toLowerCase());
										currentKey = null;
									}
									break;
								case STATE:
									if (currentKey != null && stateTemplate != null) {
										stateTemplate.add(currentKey, text.toLowerCase());
									}
									break;
								case NEXT:
									if (currentKey != null && stateTemplate != null) {
										stateTemplate.next.put(currentKey.toLowerCase(), text.toLowerCase());
									}
									break;
							}
						}
						break;
				}
				parser.next();
			}
			return template.toSprite();
		} else 
			return null;
	}
}
