package de.paeterick.tda.system.content.sprites;

import java.awt.image.BufferedImage;

import de.paeterick.piframework.game.IUpdateable;
import de.paeterick.piframework.math.SizeF;

public interface ISprite extends IUpdateable {
	void next();
	void next(final String name);
	void animate(String name);
	String getName();
	String getDescription();
	SizeF getHalfDimension();
	BufferedImage getImage();
}
