package de.paeterick.tda.system.content.sprites;

import java.util.Map;

public class State {
	private final String name;
	private final int length;
	private final int startX;
	private final int startY;
	private final int sizeX;
	private final int sizeY;
	private final int pause;
	private final AnimationMode animationMode;
	private final Map<String, String> next;
	
	public State(final String name, final int length, final int startX, final int startY, final int sizeX, final int sizeY, final int pause, final Map<String, String> next) {
		this(name, length, startX, startY, sizeX, sizeY, pause, next, AnimationMode.REPEAT);
	}
	public State(final String name, final int length, final int startX, final int startY, final int sizeX, final int sizeY, final int pause, final Map<String, String> next, final AnimationMode animationMode) {
		this.name = name;
		this.length = length;
		this.startX = startX;
		this.startY = startY;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.pause = pause;
		this.animationMode = animationMode;
		this.next = next;
	}
	
	public String getName() {
		return name;
	}
	public int getLength() {
		return length;
	}
	public int getStartX() {
		return startX;
	}
	public int getStartY() {
		return startY;
	}
	public int getSizeX() {
		return sizeX;
	}
	public int getSizeY() {
		return sizeY;
	}
	public int getPause() {
		return pause;
	}
	public AnimationMode getAnimationMode() {
		return animationMode;
	}
	public String getNextState(String name) {
		name = name.toLowerCase();
		if (next.containsKey(name))
			return next.get(name);
		return null;
	}
}
