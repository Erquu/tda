package de.paeterick.tda.system.content.sprites;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.paeterick.piframework.math.SizeF;
import de.paeterick.tda.system.content.store.ImageStore;

public class Sprite implements ISprite {
	
	private final String fName;
	private final String fDescription;
//	private final int fDefaultStateIndex;
	private final BufferedImage fSpriteImage;
	private BufferedImage fStateImage;
	private SizeF fStateSize;
	private final Map<String, State> fStateMap = new HashMap<String, State>();
	
	private String fCurrentStateName;
	private State fActiveState;
	private long fLastUpdateTime = 0;
	int fCurrentFrameNum = 0;
	boolean forward = true;
	
	public Sprite(final String name, final String description, final String imagePath, final List<State> states, final int defaultStateIndex) {
		this.fName = name;
		this.fDescription = description;
		this.fSpriteImage = ImageStore.get(imagePath);
//		this.fDefaultStateIndex = defaultStateIndex;
		for (final State state : states) {
			this.fStateMap.put(state.getName(), state);
			fCurrentStateName = state.getName();
		}
		if (defaultStateIndex > -1 && defaultStateIndex < states.size()) {
			animate(states.get(defaultStateIndex).getName());
		} else {
			animate(fCurrentStateName);
		}
		updateStateImage();
	}

	@Override
	public void update(long time) {
		if (fLastUpdateTime + fActiveState.getPause() < time) {
			fLastUpdateTime = time;
			
			final int currentFrameBefore = fCurrentFrameNum;
			
			switch (fActiveState.getAnimationMode()) {
				case BACK_AND_FORTH:
					if (forward) {
						fCurrentFrameNum++;
						if (fCurrentFrameNum >= fActiveState.getLength()) {
							forward = false;
							fCurrentFrameNum = fActiveState.getLength() - 2;
						}
					} else {
						fCurrentFrameNum--;
						if (fCurrentFrameNum <= 0) {
							forward = true;
							fCurrentFrameNum = 0;
						}
					}
					break;
				case REPEAT:
					fCurrentFrameNum++;
					if (fCurrentFrameNum >= fActiveState.getLength()) {
						fCurrentFrameNum = 0;
					}		
					break;
				case SINGLE:
					
					break;
			}
			
			if (currentFrameBefore != fCurrentFrameNum) {
				updateStateImage();
			}
		}
	}
	
	private void updateStateImage() {
		fStateImage = fSpriteImage.getSubimage(fActiveState.getStartX() + (fCurrentFrameNum * fActiveState.getSizeX()), fActiveState.getStartY(), fActiveState.getSizeX(), fActiveState.getSizeY());
	}
	
	@Override
	public void next() {
		next("default");
	}
	
	@Override
	public void next(final String name) {
		final String nextAnimationName = fActiveState.getNextState(name);
		if (nextAnimationName != null) {
			animate(nextAnimationName);
		}
	}
	
	@Override
	public void animate(String name) {
		if (name != null) {
			name = name.toLowerCase();
			if (fStateMap.containsKey(name) && !fCurrentStateName.equals(name)) {
				fCurrentFrameNum = 0;
				forward = true;
				fCurrentStateName = name;
				fActiveState = fStateMap.get(name);
				
				fStateSize = new SizeF(((float)fActiveState.getSizeX() * .5f), ((float)fActiveState.getSizeY() * .5f));
			}
		}
	}

	@Override
	public String getName() {
		return fName;
	}

	@Override
	public String getDescription() {
		return fDescription;
	}
	
	@Override
	public SizeF getHalfDimension() {
		return fStateSize;
	}

	@Override
	public BufferedImage getImage() {
		return fStateImage;
	}
}
