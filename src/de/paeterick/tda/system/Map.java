package de.paeterick.tda.system;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.List;

import de.paeterick.piframework.game.IRenderable;
import de.paeterick.piframework.game.graphics.Viewport;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.tda.system.content.pipeline.IObjectPipeline;
import de.paeterick.tda.system.content.store.ImageStore;
import de.paeterick.tda.system.objects.IGameObject;

public class Map implements IRenderable {
	
	private static final Color gridColor = new Color(200, 200, 200, 100);
	
	public static final double TILESIZE_X = 256;
	public static final double TILESIZE_Y = 256;
	
	public static final double GRIDSIZE_X = 32;
	public static final double GRIDSIZE_Y = 32;

	private final IObjectPipeline objectPipeline;
	private List<IGameObject> objectsToRender;
	
	private final int maxTilesX = 100;
	private final int maxTilesY = 100;
	
	private int offsetX = 0;
	private int offsetY = 0;
	private int countX = 0;
	private int countY = 0;
	
	private int mapX = 0;
	private int mapY = 0;
	
	private boolean drawGrid = false;
	
	public Map(final IObjectPipeline objectPipeline) {
		this.objectPipeline = objectPipeline;
	}
	
	public Vector2 toWorldCoords(final float x, final float y) {
		return new Vector2(mapX + x, mapY + y);
	}
	
	public void drawGrid(final boolean drawGrid) {
		this.drawGrid = drawGrid;
	}
	
	public void process(Viewport viewport) {
		
		mapX = viewport.x;
		mapY = viewport.y;
		
		int coordX = (int) Math.floor((double)mapX / TILESIZE_X); 
		int coordY = (int) Math.floor((double)mapY / TILESIZE_Y);
		
		int targetX = (int) Math.ceil((double)viewport.width / TILESIZE_X) + 1;
		int targetY = (int) Math.ceil((double)viewport.height / TILESIZE_Y) + 1;
		
		if (coordX < 0)
			coordX = 0;
		if (coordY < 0)
			coordY = 0;
		
		if (coordX + targetX > maxTilesX)
			targetX = maxTilesX - coordX;
		if (coordY + targetY > maxTilesY)
			targetY = maxTilesY - coordY;
		
		offsetX = (int)(mapX - (coordX * TILESIZE_X));
		offsetY = (int)(mapY - (coordY * TILESIZE_Y));
		
		countX = targetX;
		countY = targetY;

		objectsToRender = objectPipeline.getObjectsInView(viewport);
		
		//System.out.printf("Map.java - Rendering from X:%s Y:%s to X:%s Y:%s offsetX:%s offsetY:%s", coordX, coordY, targetX, targetY, offsetX, offsetY);
		//System.out.println();
	}

	@Override
	public void render(Graphics2D context) {
		final BufferedImage img = ImageStore.get("0-0.png");
		
		for (int x = 0; x < countX; x++) {
			for (int y = 0; y < countY; y++) {
				context.drawImage(img, (int)(x * TILESIZE_X - offsetX), (int)(y * TILESIZE_Y - offsetY), null);
			}
		}
		
		if (drawGrid) {
			context.setColor(gridColor);
			final int width = (int)(countX * TILESIZE_X);
			final int height = (int)(countY * TILESIZE_Y);
			for (int x = 0; x < (width / GRIDSIZE_X); x++) {
				context.drawLine((int)(x * GRIDSIZE_X) - offsetX, - offsetY, (int)(x * GRIDSIZE_X) - offsetX, height - offsetY);
			}
			for (int y = 0; y < (height / GRIDSIZE_Y); y++) {
				context.drawLine(-offsetX, (int)(y * GRIDSIZE_Y) - offsetY, width - offsetX, (int)(y * GRIDSIZE_Y) - offsetY);
			}
		}
		
		
		if (objectsToRender != null) {
			context.setColor(Color.WHITE);
			for (final IGameObject obj : objectsToRender) {
				context.drawImage(obj.getImage(),
					(int)(obj.getCenter().x - obj.getHalfDimension().width) - mapX,
					(int)(obj.getCenter().y - obj.getHalfDimension().height) - mapY,
					null);		
			}
		}
	}
	
	public int getMapX() {
		return mapX;
	}
	
	public int getMapY() {
		return mapY;
	}
}
