package de.paeterick.tda.system;

import java.awt.Graphics2D;
import java.util.List;
import java.util.Vector;

import de.paeterick.piframework.game.IRenderable;
import de.paeterick.tda.system.objects.IGameObject;

public class Renderer implements IRenderable {
	
	private final Map map;
	private final List<IGameObject> objects = new Vector<IGameObject>();
	
	public Renderer(final Map map) {
		this.map = map;
	}
	
	public void addObject(final IGameObject obj) {
		if (!objects.contains(obj)) {
			synchronized(objects) {
				objects.add(obj);
			}
		}
	}
	
	public void removeObject(final IGameObject obj) {
		if (objects.contains(obj)) {
			synchronized (objects) {
				objects.remove(obj);
			}
		}
	}

	@Override
	public void render(Graphics2D context) {
		final int mapX = map.getMapX();
		final int mapY = map.getMapY();
		
		map.render(context);
		
		synchronized(objects) {
			for (IGameObject obj : objects) {
				context.drawImage(
					obj.getImage(),
					(int)(obj.getCenter().x - obj.getHalfDimension().width - mapX),
					(int)(obj.getCenter().y - obj.getHalfDimension().height - mapY),
					null);
				context.drawRect(
					(int)(obj.getCenter().x - mapX) - 1,
					(int)(obj.getCenter().y - mapY) - 1,
					2,
					2);
			}
		}
	}

}
