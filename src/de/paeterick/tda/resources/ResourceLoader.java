package de.paeterick.tda.resources;

import de.paeterick.piframework.resources.ImageLoader;

public class ResourceLoader extends ImageLoader {
	private String rootFolder = "";
	
	public ResourceLoader() {
		
	}
	public ResourceLoader(String rootFolder) {
		this.rootFolder = rootFolder;
	}
	
	@Override
	public void setFile(String path) {
		super.setFile(rootFolder.concat(path));
	}
}
