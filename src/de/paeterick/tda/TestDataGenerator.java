package de.paeterick.tda;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import de.paeterick.piframework.math.MathHelper;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.tda.system.content.database.DatabaseConnection;

public class TestDataGenerator {
	public static void generateTrees(final DatabaseConnection connection, final int count) {
		try {
			final String[] plants = new String[] {
				"plants/tree_01.png",
				"plants/tree_02.png",
				"plants/tree_03.png",
				"plants/tree_04.png",
				"plants/tree_05.png",
				"plants/tree_06.png"
			};
			
			PreparedStatement ps = connection.getConnection().prepareStatement("INSERT INTO OBJECTS (POSX, POSY, FILEPATH, HEALTH, INVINCIBLE) VALUES (?, ?, ?, ?, ?)");
			Vector2 pos;
			for (int i = 0; i < count; i++) {
				pos = MathHelper.randomVector2(0, 25600, 0, 25600);
				ps.setInt(1, (int)pos.x);
				ps.setInt(2, (int)pos.y);
				ps.setString(3, plants[MathHelper.randomInteger(0, plants.length)]);
				ps.setInt(4, 100);
				ps.setInt(5, 1);
				ps.addBatch();
			}
			
			connection.getConnection().setAutoCommit(false);
			ps.executeBatch();
			connection.getConnection().setAutoCommit(true);
		} catch (SQLException e) {
			
		}
	}
}
