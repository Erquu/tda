package de.paeterick.tda;

import java.awt.Graphics2D;
import java.sql.SQLException;

import javax.xml.stream.XMLStreamException;

import de.paeterick.piframework.game.Game;
import de.paeterick.piframework.game.GameEngine;
import de.paeterick.piframework.game.GameFactory;
import de.paeterick.piframework.game.PresentParameters;
import de.paeterick.piframework.game.console.ConsoleActionArguments;
import de.paeterick.piframework.game.console.IConsoleAction;
import de.paeterick.piframework.game.exceptions.EngineAlreadyStartedException;
import de.paeterick.piframework.game.graphics.Camera;
import de.paeterick.piframework.game.graphics.Viewport;
import de.paeterick.piframework.hid.Mouse;
import de.paeterick.piframework.math.SizeF;
import de.paeterick.piframework.math.Vector2;
import de.paeterick.tda.resources.ResourceLoader;
import de.paeterick.tda.system.Map;
import de.paeterick.tda.system.Renderer;
import de.paeterick.tda.system.content.database.DatabaseConnection;
import de.paeterick.tda.system.content.pipeline.IObjectPipeline;
import de.paeterick.tda.system.content.pipeline.PreloadingDbObjectPipeline;
import de.paeterick.tda.system.content.sprites.ISprite;
import de.paeterick.tda.system.content.sprites.NoSprite;
import de.paeterick.tda.system.content.sprites.Spriteloader;
import de.paeterick.tda.system.content.store.ImageStore;
import de.paeterick.tda.system.objects.Player;
import de.paeterick.tda.system.objects.StaticGameObject;

public class TDA extends Game implements IConsoleAction {

	public static GameEngine ENGINE;
	
	public static void main(String[] args) throws EngineAlreadyStartedException {
		final PresentParameters pp = new PresentParameters();
		
		TDA.ENGINE = GameFactory.create(new TDA(), pp);
		TDA.ENGINE.start();
	}
	
	private final ResourceLoader resourceLoader = new ResourceLoader("de/paeterick/tda/resources/");
	private DatabaseConnection connection;
	private IObjectPipeline objectPipeline;
	private Map map;
	private Renderer renderer;
	private Camera camera;
	private Player player;
	private ISprite test;
	
	private boolean creationMode = false;
	private StaticGameObject sgo = null;
	private String sgoPath = null;

	@Override
 	public void init() {
		getEngine().setResizable(true);
		getEngine().showDebugOuput(true);
		
		ImageStore.initialize(resourceLoader);
		Spriteloader.initialize(resourceLoader);
		
		try {
			test = Spriteloader.load("xml/magickid.xml");
			if (test == null)
				test = new NoSprite();
			test.animate("idle_down");
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void load() {
		
		connection = new DatabaseConnection(getEngine().getConsole(), resourceLoader);
		
		player = new Player(test);
		player.setCenter(new Vector2(1000, 1000));
		
		camera = new Camera(
			new Viewport(0, 0, getClientRect().width, getClientRect().height),
			Vector2.emptyVector()
		);
		camera.moveTo(player.getCenter().x, player.getCenter().y);
		
		objectPipeline = new PreloadingDbObjectPipeline(connection);
		map = new Map(objectPipeline);
		renderer = new Renderer(map);
		
		map.process(camera.getViewport());
		objectPipeline.forceReload(3);
		
		renderer.addObject(player);
		
		getEngine().getConsole().addAction("teleport", this); // Admin function
		getEngine().getConsole().addAction("map", this); // Admin function
		getEngine().getConsole().addAction("create", this); // Admin function
	}

	@Override
	public void update(final long time) {
		
		final float mx = getMouseState().getX() - getClientRect().x;
		final float my = getMouseState().getY() - getClientRect().y;
		
		if (getMouseState().isPressed(Mouse.RIGHT)) {
			final Vector2 mWorld = map.toWorldCoords(mx, my);
			player.moveTo(mWorld);
		}
		
		if (creationMode) {
			Vector2 mWorld = map.toWorldCoords(mx, my);
			mWorld.x = (int)(Math.floor(mWorld.x / Map.GRIDSIZE_X) * Map.GRIDSIZE_X);
			mWorld.y = (int)(Math.floor(mWorld.y / Map.GRIDSIZE_Y) * Map.GRIDSIZE_Y);
			sgo.setCenter(mWorld);
			if (getMouseState().isPressed(Mouse.LEFT)) {
				creationMode = false;
				try {
					connection.executeUpdate("INSERT INTO OBJECTS (POSX, POSY, FILEPATH, HEALTH, INVINCIBLE) VALUES (%s, %s, '%s', 100, 1)", sgo.getCenter().x, sgo.getCenter().y, sgoPath);
					objectPipeline.forceReload(3);
				} catch (SQLException e) {
					e.printStackTrace();
					getEngine().getConsole().addMessage("[^2]! Creation failed: %s", e.getMessage());
				}
				renderer.removeObject(sgo);
			}
		}
		
		player.update(time);
		camera.moveTo(player.getCenter().x, player.getCenter().y);
		
		map.process(camera.getViewport());
	}

	@Override
	public void render(final Graphics2D context) {
		renderer.render(context);
	}
	
	@Override
	public void postrender(final Graphics2D context) {
		context.drawRect((int)getMouseState().getX() - 5, (int)getMouseState().getY() - 5, 10, 10);
	}
	
	@Override
	public void resize(final SizeF newSize) {
		camera.getViewport().width = (int)newSize.width;
		camera.getViewport().height = (int)newSize.height;
	}

	@Override
	public void unload() {
		
	}

	@Override
	public void doAction(final ConsoleActionArguments args) {
		if ("teleport".equals(args.getCommand()) && args.getArgs().length >= 2) {
			int x = Integer.parseInt(args.getArgs()[0]);
			int y = Integer.parseInt(args.getArgs()[1]);
			
			getEngine().getConsole().addMessage("[^3]Teleporting to X:%s Y:%s", x, y);
			
			player.setCenter(new Vector2(x, y));
			camera.moveTo(player.getCenter().x, player.getCenter().y);
			
			map.process(camera.getViewport());
			objectPipeline.forceReload(3);
		} else if ("map".equals(args.getCommand())) {
			if (args.getArgs().length >= 1) {
				if ("reload".equals(args.getArgs()[0])) {
					objectPipeline.forceReload(1);
				} else if ("grid".equals(args.getArgs()[0])) {
					map.drawGrid(((args.getArgs().length == 2) ? Boolean.parseBoolean(args.getArgs()[1]) : true));
				}	
			}
		} else if ("create".equals(args.getCommand())) {
			if (args.getArgs().length >= 2) {
				sgoPath = args.getArgs()[0].concat("/").concat(args.getArgs()[1]).concat(".png");
				sgo = new StaticGameObject(sgoPath);
				renderer.addObject(sgo);
				creationMode = true;
			}
		} else if ("commit".equals(args.getCommand())) {
			try {
				connection.getConnection().commit();
				getEngine().getConsole().addMessage("[^3] Commit successful");
			} catch (SQLException e) {
				e.printStackTrace();
				getEngine().getConsole().addMessage("[^2]! Commit failed: %s", e.getMessage());
			}
		}
	}

	@Override
	public String getSuggestion(final ConsoleActionArguments args) {
		if ("teleport".equals(args.getCommand())) {
			return "teleport <x> <y>";
		} else if ("map".equals(args.getCommand())) {
			if (args.getArgs().length >= 1) {
				
				if (args.getArgs()[0].startsWith("r")) {
					return "map reload";
				} else if (args.getArgs()[0].startsWith("g")) {
					return "map grid [bool]";
				}	
			}
		}
		
		return "";
	}

}
