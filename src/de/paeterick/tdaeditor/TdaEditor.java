package de.paeterick.tdaeditor;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import de.paeterick.piframework.game.Game;
import de.paeterick.piframework.game.GameEngine;
import de.paeterick.piframework.game.GameFactory;
import de.paeterick.piframework.game.exceptions.EngineAlreadyStartedException;
import de.paeterick.piframework.game.graphics.Camera;
import de.paeterick.tda.resources.ResourceLoader;
import de.paeterick.tda.system.Map;
import de.paeterick.tda.system.Renderer;
import de.paeterick.tda.system.content.database.DatabaseConnection;
import de.paeterick.tda.system.content.pipeline.IObjectPipeline;
import de.paeterick.tda.system.content.sprites.Sprite;
import de.paeterick.tda.system.content.sprites.Spriteloader;
import de.paeterick.tda.system.content.store.ImageStore;
import de.paeterick.tda.system.objects.Player;

public class TdaEditor extends Game {
	
	private static GameEngine ENGINE;

	public static void main(String[] args) throws InterruptedException, InvocationTargetException, EngineAlreadyStartedException {
		ENGINE = GameFactory.create(new TdaEditor());
		
		final TileWindow tileWindow = new TileWindow();
		
		SwingUtilities.invokeAndWait(new Runnable() {
			@Override
			public void run() {
				//tileWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				tileWindow.setPreferredSize(new Dimension(255, 800));
				tileWindow.setVisible(true);
				tileWindow.pack();
			}
		});
		
		ENGINE.start();
	}

	private final ResourceLoader resourceLoader = new ResourceLoader("de/paeterick/tda/resources/");
	private DatabaseConnection connection;
	private IObjectPipeline objectPipeline;
	private Map map;
	private Renderer renderer;
	private Camera camera;
	private Player player;
	private Sprite test;
	
	public TdaEditor() {
		
	}
	
	private JMenuBar createMenubar() {
		JMenuBar menubar = new JMenuBar();
		
		JMenu fileMenu = new JMenu("File");
		JMenuItem newItem = new JMenuItem("New Map");
		fileMenu.add(newItem);
		menubar.add(fileMenu);
		
		return menubar;
	}

	@Override
	public void init() {
		getEngine().setResizable(true);
		getEngine().showDebugOuput(true);
		
		ImageStore.initialize(resourceLoader);
		Spriteloader.initialize(resourceLoader);
	}
	
	@Override
	public void postinit() {
		
	}

	@Override
	public void load() {
		
	}
	
	@Override
	public void update(long time) {
		
	}

	@Override
	public void render(Graphics2D context) {
		
	}

	@Override
	public void unload() {
		
	}
}
