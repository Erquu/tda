package de.paeterick.tdaeditor.components;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

public class JTileButton extends JButton {
	
	private final String name;
	
	public JTileButton(String name) {
		super();
		this.name = name;
		setBorder(null);
	}
	public JTileButton(String name, Action a) {
		super(a);
		this.name = name;
		setBorder(null);
	}
	public JTileButton(String name, Icon icon) {
		super(icon);
		this.name = name;
		setBorder(null);
	}
	public JTileButton(String name, String text) {
		super(text);
		this.name = name;
		setBorder(null);
	}
	public JTileButton(String name, String text, Icon icon) {
		super(text, icon);
		this.name = name;
		setBorder(null);
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getSizeString(final String format) {
		return String.format(format, getIcon().getIconWidth(), getIcon().getIconHeight());
	}
}
