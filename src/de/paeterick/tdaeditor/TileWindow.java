package de.paeterick.tdaeditor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.List;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import de.paeterick.tdaeditor.components.JTileButton;

public class TileWindow extends JFrame implements ActionListener, TreeSelectionListener {
	private static final long serialVersionUID = 2198494867711312982L;
	
	//private static String IMAGEPATH = "C:\\Users\\csp8fe\\mvnworkspace\\TDA\\src\\de\\paeterick\\tda\\resources";
	//private static String IMAGEPATH = "/Users/Erquu/Java/workspace/TDA/src/de/paeterick/tda/resources";
	private static String IMAGEPATH = "/Users/Erquu/Desktop/TDA";
	
	private DefaultMutableTreeNode fCategoryRootNode;
	private JScrollPane fScrollPane;
	private JPanel fScrollContent;
	private final JLabel fDetailNameLabel = new JLabel("undefined");
	private final JLabel fDetailSizeLabel = new JLabel("0x0 px");
	private final JLabel fDetaulPreviewLabel = new JLabel("No Preview");
	
	public TileWindow() {
		
		final JPanel mainPane = new JPanel(new BorderLayout());
		//mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.Y_AXIS));
		
		// MAIN GUI
		
		mainPane.add(createCategoryPane(), BorderLayout.PAGE_START);
		mainPane.add(createScrollPane(), BorderLayout.CENTER);
		mainPane.add(createPreviewPane(), BorderLayout.PAGE_END);
		
		// END MAIN GUI
		
		final JPanel borderYPane = new JPanel();
		borderYPane.setLayout(new BoxLayout(borderYPane, BoxLayout.Y_AXIS));
		final JPanel borderXPane = new JPanel();
		borderXPane.setLayout(new BoxLayout(borderXPane, BoxLayout.X_AXIS));
		
		borderXPane.add(new Box.Filler(new Dimension(0, 0), new Dimension(10, 0), new Dimension(10, 0)));
		borderXPane.add(mainPane);
		borderXPane.add(new Box.Filler(new Dimension(0, 0), new Dimension(10, 0), new Dimension(10, 0)));
		
		borderYPane.add(new Box.Filler(new Dimension(0, 0), new Dimension(0, 10), new Dimension(0, 10)));
		borderYPane.add(borderXPane);
		borderYPane.add(new Box.Filler(new Dimension(0, 0), new Dimension(0, 10), new Dimension(0, 10)));
		
		loadCategories();
		
		this.getContentPane().add(borderYPane);
	}
	
	private JScrollPane createCategoryPane() {
		fCategoryRootNode = new DefaultMutableTreeNode("Tilesets");
		final JTree tree = new JTree(fCategoryRootNode);
		tree.addTreeSelectionListener(this);
		final JScrollPane scrollPane = new JScrollPane(tree);
		scrollPane.setPreferredSize(new Dimension(200, 200));
		
		return scrollPane;
	}
	
	private JPanel createScrollPane() {
		final JPanel scrollRootPane = new JPanel();
		scrollRootPane.setLayout(new BoxLayout(scrollRootPane, BoxLayout.Y_AXIS));
		fScrollContent = new JPanel(new ModifiedFlowLayout());
		fScrollPane = new JScrollPane(fScrollContent);
		fScrollPane.setPreferredSize(new Dimension(200, 300));
		
		scrollRootPane.add(new Box.Filler(new Dimension(0, 0), new Dimension(0, 10), new Dimension(0, 10)));
		scrollRootPane.add(fScrollPane);
		scrollRootPane.add(new Box.Filler(new Dimension(0, 0), new Dimension(0, 10), new Dimension(0, 10)));
		
		return scrollRootPane;
	}
	
	private JPanel createPreviewPane() {
		final JPanel previewPane = new JPanel(new BorderLayout(10, 0));
		previewPane.add(fDetaulPreviewLabel, BorderLayout.LINE_START);
		
		final JPanel detailsPane = new JPanel();
		detailsPane.setLayout(new BoxLayout(detailsPane, BoxLayout.Y_AXIS));
		
		detailsPane.add(createNamedLabel("Name:", fDetailNameLabel));
		detailsPane.add(createNamedLabel("Size:", fDetailSizeLabel));
		
		previewPane.add(detailsPane, BorderLayout.CENTER);
		
		return previewPane;
	}
	
	private JPanel createNamedLabel(final String name, final JLabel label) {
		final JPanel pane = new JPanel(new BorderLayout(5, 0));
		pane.add(new JLabel(name), BorderLayout.LINE_START);
		pane.add(label, BorderLayout.CENTER);
		return pane;
	}

	private void loadCategories() {
		fCategoryRootNode.removeAllChildren();
		listFolders(fCategoryRootNode, new File(IMAGEPATH));
	}
	
	private boolean listFolders(final DefaultMutableTreeNode node, final File root) {

		boolean foundIgnoreFile = false;
		if (root.isDirectory()) {
			DefaultMutableTreeNode subNode;
			
			for (final File file : root.listFiles()) {
				if (file.isDirectory()) {
					subNode = new DefaultMutableTreeNode(file.getName());
					listFolders(subNode, file);
					node.add(subNode);
					if (file.getName().toLowerCase().equals(".ignore")) {
						foundIgnoreFile = true;
						break;
					}
				}
			}
		}
		return foundIgnoreFile;
	}
	
	private List<String> getImages(final File path) {
		List<String> imagePaths = new Vector<String>();
		if (path.isDirectory()) {
			for (final String file : path.list(new FilenameFilter() {
				@Override
				public boolean accept(File arg0, String arg1) {
					return (arg1.endsWith(".png") || arg1.endsWith(".jpg") || arg1.endsWith(".jpeg"));
				}
			})) {
				imagePaths.add(file);
			}
		}
		return imagePaths;
	}
	
	@Override
	public void actionPerformed(final ActionEvent arg0) {
		/*if (arg0.getSource().equals(fCategoryList)) {
			fScrollContent.removeAll();
			final File file = new File(IMAGEPATH.concat(fCategoryList.getSelectedItem().toString()));
			final MouseAdapter buttonListener = new MouseAdapter() {
				@Override
				public void mouseEntered(MouseEvent e) {
					final Object obj = e.getSource();
					if (obj instanceof JTileButton) {
						final JTileButton btn = (JTileButton)obj;
						fDetailNameLabel.setText(btn.getName());
						fDetailSizeLabel.setText(btn.getSizeString("%sx%s px"));
						fDetaulPreviewLabel.setText("");
						fDetaulPreviewLabel.setIcon(btn.getIcon());
					}
				}
			};
			String name;
			JTileButton button;
			for (final String path : getImages(file)) {
				name = path.substring(0, path.lastIndexOf("."));
				button = new JTileButton(name, new ImageIcon(String.format("%s%s%s", file.getAbsolutePath(), File.separator, path)));
				//button.setPreferredSize(new Dimension(32, 32));
				button.addMouseListener(buttonListener);
				fScrollContent.add(button);
			}
			fScrollContent.revalidate();
			fScrollPane.revalidate();
		}*/
	}

	@Override
	public void valueChanged(TreeSelectionEvent e) {
		TreePath test = e.getNewLeadSelectionPath();
		final StringBuilder b = new StringBuilder();
		b.append(IMAGEPATH);
		b.append(File.separator);
		for (Object t : test.getPath()) {
			b.append(t.toString());
			b.append(File.separator);
		}
		
		fScrollContent.removeAll();
		final File file = new File(b.toString());
		System.out.println(file.getAbsolutePath());
		final MouseAdapter buttonListener = new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				final Object obj = e.getSource();
				if (obj instanceof JTileButton) {
					final JTileButton btn = (JTileButton)obj;
					fDetailNameLabel.setText(btn.getName());
					fDetailSizeLabel.setText(btn.getSizeString("%sx%s px"));
					fDetaulPreviewLabel.setText("");
					fDetaulPreviewLabel.setIcon(btn.getIcon());
				}
			}
		};
		String name;
		JTileButton button;
		for (final String path : getImages(file)) {
			System.out.println(path);
			name = path.substring(0, path.lastIndexOf("."));
			button = new JTileButton(name, new ImageIcon(String.format("%s%s%s", file.getAbsolutePath(), File.separator, path)));
			//button.setPreferredSize(new Dimension(32, 32));
			button.addMouseListener(buttonListener);
			fScrollContent.add(button);
		}
		fScrollContent.revalidate();
		fScrollPane.revalidate();
	}
}
